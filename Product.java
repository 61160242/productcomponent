/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author lovel
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Moca1", 40, "1.png"));
        list.add(new Product(2, "Moca2", 40, "2.jpg"));
        list.add(new Product(3, "Moca3", 40, "1.png"));
        list.add(new Product(4, "Milk1", 20, "2.jpg"));
        list.add(new Product(5, "Milk2", 20, "1.png"));
        list.add(new Product(6, "Milk3", 20, "2.jpg"));
        list.add(new Product(7, "Mocha1", 50, "1.png"));
        list.add(new Product(8, "Mocha2", 50, "2.jpg"));
        return list;
    }
}
